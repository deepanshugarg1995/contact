import React from "react";
import { Component } from "react";
import "./Contact.css";
class Contact extends Component {
  state = {
    data: []
  };
  addUser = e => {
    e.preventDefault();
    console.log(e.target.value, e.target.name.value);
    let data = [];
    data["name"] = e.target.name.value;
    data["age"] = e.target.age.value;
    data["gender"] = e.target.gender.value;
    data["company"] = e.target.company.value;
    data["mobile"] = e.target.mobile.value;
    console.log(data);
    let raw = this.state.data;
    raw.push(data);

    this.setState({
      data: raw
    });
  };
  render() {
    let comp = "";
    if (this.state.data != []) {
      comp = this.state.data.map(val => {
        return (
          <div>
            <img src="./assests/images/img_avatar2.png" alt="Avatar Woman" />
            <h2> {val["name"]} </h2>
            <p>Some text here too.</p>
          </div>
        );
      });
    }

    return (
      <div>
        <div className="split left">
          <h1 align="center">Add Contact</h1>
          <div>
            <div className="left">
              <form
                onSubmit={e => {
                  this.addUser(e);
                }}
              >
                <fieldset>
                  <legend>Personal information:</legend>
                  Name:
                  <input
                    type="text"
                    name="name"
                    placeholder="Enter your name"
                  />
                  <br />
                  <br />
                  Age:
                  <input type="text" name="age" placeholder="Age" />
                  <br />
                  <br />
                  Gender
                  <select name="gender">
                    <option value="Male" selected>
                      Male
                    </option>
                    <option value="Female">Female</option>
                    <option value="Others">Others</option>
                  </select>
                  <br />
                  <br />
                  Company:
                  <input type="text" name="company" placeholder="MuSigma" />
                  <br />
                  <br />
                  Mobile:
                  <input type="text" name="mobile" placeholder="10 digits" />
                  <br />
                  <br />
                  <input type="submit" value="Submit" />
                </fieldset>
              </form>
            </div>
          </div>
        </div>
        {/* now right */}
        <div className="split right">
          <div className="right">
            <div className="savedContacts" style={{ margin: "5px" }}>
              <h1 align="center">Saved Contacts</h1>
              <div style={{ padding: "20px" }} className="header">
                <img
                  src="./assests/images/img_avatar2.png"
                  alt="Avatar Woman"
                />
                <h2>John Doe</h2>
                <p>Some text here too.</p>
                <br />
                <img src="./assests/images/img_avatar.png" alt="Avatar Woman" />
                <h2>John Doe</h2>
                <p>Some text here too.</p>
                {comp}
                {/* 
                <img
                  src="./assests/images/img_avatar2.png"
                  alt="Avatar Woman"
                />
                <h2>John Doe</h2>
                <p>Some text here too.</p> */}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Contact;
